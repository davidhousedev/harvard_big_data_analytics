import re
import string

from pyspark import SparkContext

MATCH_PROG = re.compile(r'\d{2}:\d{3}:\d{3}')



def main():
    sc = SparkContext(appName='MyApp')

    # Load stop words into RDD
    stop_words_file = sc.textFile('stop_words.txt')
    stop_words = stop_words_file.filter(lambda line: line)

    # Load bible into RDD of words in bible
    bible_file = sc.textFile('bible.txt')
    bible_words = clean_text_file(bible_file, stop_words)
    bible_words = remove_verse_numbers(bible_words)

    # Load Joyce file into RDD of words
    joyce_file = sc.textFile('4300.txt')
    joyce_words = clean_text_file(joyce_file, stop_words)

    print('Creating RDD with words common in both texts...')
    bible_word_counts = bible_words.map(lambda word: (word, 1))
    bible_aggregates = bible_word_counts.reduceByKey(lambda x, y: x + y)
    print('\n30 most frequent words found in Bible:')
    print(list_most_frequent_words(bible_aggregates))

    joyce_word_counts = joyce_words.map(lambda word: (word, 1))
    joyce_aggregates = joyce_word_counts.reduceByKey(lambda x, y: x + y)
    print('\n30 most frequent words found in James Joyce:')
    print(list_most_frequent_words(joyce_aggregates))

    joined_words = bible_aggregates.join(joyce_aggregates)
    joined_words = joined_words.map(
        lambda key_val: (key_val[0], key_val[1][0], key_val[1][1]))

    most_common_shared = joined_words.takeOrdered(
        20, key = lambda key_val: -(key_val[1] + key_val[2]))
    print('\n20 Most common words across both texts')
    for word, bible_times, joyce_times in most_common_shared:
        print('Word: {}, '
              'Total usages: {}, '
              'Bible usages: {}, '
              'Joyce usages: {}'.format(word,
                                        (bible_times + joyce_times),
                                        bible_times,
                                        joyce_times))

    print('\nRe-display same data, in descending sort by word usage in Joyce')
    for word, bible_times, joyce_times in sorted(most_common_shared,
                                                 key=lambda tup: -tup[2]):
        print('Word: {}, '
              'Bible usages: {}, '
              'Joyce usages: {}'.format(word,
                                        bible_times,
                                        joyce_times))

    print('\nRe-display same data, in descending sort by word usage in Bible')
    for word, bible_times, joyce_times in sorted(most_common_shared,
                                                 key=lambda tup: -tup[1]):
        print('Word: {}, '
              'Bible usages: {}, '
              'Joyce usages: {}'.format(word,
                                        bible_times,
                                        joyce_times))

    print('\nPrinting random 5% sample of words common in both texts')
    five_percent = int(joined_words.count() * 0.05)
    print([word for word, _, _ in
           joined_words.takeSample(False, five_percent)])


def list_most_frequent_words(words_rdd):
    most_frequent = words_rdd.takeOrdered(30, key = lambda key_val: -key_val[1])
    for word, n_times in most_frequent:
        print('Word: {}, Usages: {}'.format(word, n_times))


def clean_text_file(text_rdd, stop_words):
    words_rdd = text_rdd.flatMap(lambda line: line.strip().split(' '))
    lowercase_rdd = words_rdd.map(lambda word: word.lower())
    lowercase_rdd = lowercase_rdd.map(strip_punctuation)
    lowercase_rdd = remove_stop_words(lowercase_rdd, stop_words)
    return lowercase_rdd.filter(lambda word: word)


def remove_stop_words(words_rdd, stop_words):
    return words_rdd.subtract(stop_words)


def remove_verse_numbers(words_rdd):
    return words_rdd.filter(lambda word: not MATCH_PROG.match(word))


def strip_punctuation(word):
    for punctuation in string.punctuation:
        word = word.strip(punctuation)
    return word

if __name__ == '__main__':
    main()
