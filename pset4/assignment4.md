HU Extension                     Assignment 04           E63 Big Data Analytics

Issued on: Sept 24, 2017                    Due on Saturday by 4 PM EST, Sept 30, 2017

You can do these problems in the language of your choice: Python, Scala, Java or R. 

### Problem 1.
Consider two attached text files: bible.txt and 4300.txt. The first contains ASCII text of King James Bible and the other the text of James Joyce’s novel Ulysses. Use Spark transformation and action functions present in RDD API to transform those texts into RDD-s that contain words and numbers of occurrence of those words in respective text. From King James Bible eliminate all verse numbers of the form: 03:019:024. Eliminate from both RDDs so called “stop words”. Please use the list of stop words on Web page: http://www.lextek.com/manuals/onix/stopwords1.html.  . Create RDD-s that contain only words unique for each of text. Finally create an RDD that contains only the words common to both texts. In latest RDD preserve numbers of occurrences in two texts. In other words a row in your RDD will look like (love 45 32). List for us 30 most frequent words in each RDD (text). Print or store the words and the numbers of occurrences. Create for us the list of 20 most frequently used words common to both texts. In your report, print (store) the words, followed by the number of occurrences in Ulysses and then the Bible. Order your report in descending order starting by the number of occurrences in Ulysses. Present the same data this time ordered by the number of occurrences in the Bible. List for us a random samples containing 5% of words in the final RDD. We are just practicing RDD transformations and actions. You could implement this problem in a command shell or as a standalone program.
(30%)

#### Explanation
For this solution, I started by loading the text files into memory as
RDDs, separating text lines into words, and ensuring that the words
were free of extraneous punctuation. I used filter and Python's Regexp
package to filter scripture numbers.

Unfortunately, there appears to be a bug in the way I handled
printing 30 most-used words from each individual text. It does not
display the most-used word in either case. I did not have enough time
to debug this.

I used `map` and `reduceByKey` to count the frequency of word
appearances, and built a three-tuple key-val RDD by joining the two
aggregation RDDs. I made use of `takeOrdered` throughout to extract
sorted collections from RDDs. Finally, I used `takeSample` to
take a random sample of 5% of the words found in both texts.

#### problem1.py
```python
import re
import string

from pyspark import SparkContext

MATCH_PROG = re.compile(r'\d{2}:\d{3}:\d{3}')



def main():
    sc = SparkContext(appName='MyApp')

    # Load stop words into RDD
    stop_words_file = sc.textFile('stop_words.txt')
    stop_words = stop_words_file.filter(lambda line: line)

    # Load bible into RDD of words in bible
    bible_file = sc.textFile('bible.txt')
    bible_words = clean_text_file(bible_file, stop_words)
    bible_words = remove_verse_numbers(bible_words)

    # Load Joyce file into RDD of words
    joyce_file = sc.textFile('4300.txt')
    joyce_words = clean_text_file(joyce_file, stop_words)

    print('Creating RDD with words common in both texts...')
    bible_word_counts = bible_words.map(lambda word: (word, 1))
    bible_aggregates = bible_word_counts.reduceByKey(lambda x, y: x + y)
    print('\n30 most frequent words found in Bible:')
    print(list_most_frequent_words(bible_aggregates))

    joyce_word_counts = joyce_words.map(lambda word: (word, 1))
    joyce_aggregates = joyce_word_counts.reduceByKey(lambda x, y: x + y)
    print('\n30 most frequent words found in James Joyce:')
    print(list_most_frequent_words(joyce_aggregates))

    joined_words = bible_aggregates.join(joyce_aggregates)
    joined_words = joined_words.map(
        lambda key_val: (key_val[0], key_val[1][0], key_val[1][1]))

    most_common_shared = joined_words.takeOrdered(
        20, key = lambda key_val: -(key_val[1] + key_val[2]))
    print('\n20 Most common words across both texts')
    for word, bible_times, joyce_times in most_common_shared:
        print('Word: {}, '
              'Total usages: {}, '
              'Bible usages: {}, '
              'Joyce usages: {}'.format(word,
                                        (bible_times + joyce_times),
                                        bible_times,
                                        joyce_times))

    print('\nRe-display same data, in descending sort by word usage in Joyce')
    for word, bible_times, joyce_times in sorted(most_common_shared,
                                                 key=lambda tup: -tup[2]):
        print('Word: {}, '
              'Bible usages: {}, '
              'Joyce usages: {}'.format(word,
                                        bible_times,
                                        joyce_times))

    print('\nRe-display same data, in descending sort by word usage in Bible')
    for word, bible_times, joyce_times in sorted(most_common_shared,
                                                 key=lambda tup: -tup[1]):
        print('Word: {}, '
              'Bible usages: {}, '
              'Joyce usages: {}'.format(word,
                                        bible_times,
                                        joyce_times))

    print('\nPrinting random 5% sample of words common in both texts')
    five_percent = int(joined_words.count() * 0.05)
    print([word for word, _, _ in
           joined_words.takeSample(False, five_percent)])


def list_most_frequent_words(words_rdd):
    most_frequent = words_rdd.takeOrdered(30, key = lambda key_val: -key_val[1])
    for word, n_times in most_frequent:
        print('Word: {}, Usages: {}'.format(word, n_times))


def clean_text_file(text_rdd, stop_words):
    words_rdd = text_rdd.flatMap(lambda line: line.strip().split(' '))
    lowercase_rdd = words_rdd.map(lambda word: word.lower())
    lowercase_rdd = lowercase_rdd.map(strip_punctuation)
    lowercase_rdd = remove_stop_words(lowercase_rdd, stop_words)
    return lowercase_rdd.filter(lambda word: word)


def remove_stop_words(words_rdd, stop_words):
    return words_rdd.subtract(stop_words)


def remove_verse_numbers(words_rdd):
    return words_rdd.filter(lambda word: not MATCH_PROG.match(word))


def strip_punctuation(word):
    for punctuation in string.punctuation:
        word = word.strip(punctuation)
    return word

if __name__ == '__main__':
    main()
```

#### Output
```bash
root@sandbox:/usr_data# python problem1.py
[...]

30 most frequent words found in Bible:
Word: lord, Usages: 7830
Word: thou, Usages: 5474
Word: thy, Usages: 4600
Word: god, Usages: 4443
Word: ye, Usages: 3982
Word: thee, Usages: 3826
Word: israel, Usages: 2565
Word: son, Usages: 2370
Word: king, Usages: 2270
Word: hath, Usages: 2264
Word: people, Usages: 2145
Word: house, Usages: 2024
Word: children, Usages: 1802
Word: day, Usages: 1734
Word: land, Usages: 1718
Word: shalt, Usages: 1616
Word: hand, Usages: 1466
Word: saying, Usages: 1445
Word: behold, Usages: 1326
Word: saith, Usages: 1262
Word: sons, Usages: 1089
Word: hast, Usages: 1070
Word: david, Usages: 1015
Word: earth, Usages: 987
Word: jesus, Usages: 983
Word: father, Usages: 979
Word: thine, Usages: 938
Word: name, Usages: 930
Word: thereof, Usages: 906

30 most frequent words found in James Joyce:
Word: stephen, Usages: 1511
Word: time, Usages: 1137
Word: yes, Usages: 1082
Word: eyes, Usages: 987
Word: hand, Usages: 915
Word: street, Usages: 876
Word: little, Usages: 870
Word: father, Usages: 828
Word: day, Usages: 747
Word: round, Usages: 717
Word: night, Usages: 696
Word: head, Usages: 666
Word: sir, Usages: 657
Word: god, Usages: 654
Word: name, Usages: 645
Word: look, Usages: 594
Word: life, Usages: 583
Word: john, Usages: 579
Word: don't, Usages: 563
Word: poor, Usages: 558
Word: woman, Usages: 558
Word: tell, Usages: 532
Word: voice, Usages: 531
Word: dedalus, Usages: 522
Word: house, Usages: 511
Word: hat, Usages: 504
Word: course, Usages: 498
Word: left, Usages: 495
Word: white, Usages: 489

20 Most common words across both texts
Word: unto, Total usages: 9012, Bible usages: 8997, Joyce usages: 15
Word: lord, Total usages: 8277, Bible usages: 7830, Joyce usages: 447
Word: thou, Total usages: 5635, Bible usages: 5474, Joyce usages: 161
Word: god, Total usages: 5097, Bible usages: 4443, Joyce usages: 654
Word: thy, Total usages: 4741, Bible usages: 4600, Joyce usages: 141
Word: ye, Total usages: 4027, Bible usages: 3982, Joyce usages: 45
Word: thee, Total usages: 3919, Bible usages: 3826, Joyce usages: 93
Word: son, Total usages: 2696, Bible usages: 2370, Joyce usages: 326
Word: israel, Total usages: 2589, Bible usages: 2565, Joyce usages: 24
Word: house, Total usages: 2535, Bible usages: 2024, Joyce usages: 511
Word: day, Total usages: 2481, Bible usages: 1734, Joyce usages: 747
Word: king, Total usages: 2467, Bible usages: 2270, Joyce usages: 197
Word: people, Total usages: 2382, Bible usages: 2145, Joyce usages: 237
Word: hand, Total usages: 2381, Bible usages: 1466, Joyce usages: 915
Word: hath, Total usages: 2303, Bible usages: 2264, Joyce usages: 39
Word: land, Total usages: 1967, Bible usages: 1718, Joyce usages: 249
Word: children, Total usages: 1961, Bible usages: 1802, Joyce usages: 159
Word: father, Total usages: 1807, Bible usages: 979, Joyce usages: 828
Word: time, Total usages: 1760, Bible usages: 623, Joyce usages: 1137
Word: shalt, Total usages: 1619, Bible usages: 1616, Joyce usages: 3

Re-display same data, in descending sort by word usage in Joyce
Word: time, Bible usages: 623, Joyce usages: 1137
Word: hand, Bible usages: 1466, Joyce usages: 915
Word: father, Bible usages: 979, Joyce usages: 828
Word: day, Bible usages: 1734, Joyce usages: 747
Word: god, Bible usages: 4443, Joyce usages: 654
Word: house, Bible usages: 2024, Joyce usages: 511
Word: lord, Bible usages: 7830, Joyce usages: 447
Word: son, Bible usages: 2370, Joyce usages: 326
Word: land, Bible usages: 1718, Joyce usages: 249
Word: people, Bible usages: 2145, Joyce usages: 237
Word: king, Bible usages: 2270, Joyce usages: 197
Word: thou, Bible usages: 5474, Joyce usages: 161
Word: children, Bible usages: 1802, Joyce usages: 159
Word: thy, Bible usages: 4600, Joyce usages: 141
Word: thee, Bible usages: 3826, Joyce usages: 93
Word: ye, Bible usages: 3982, Joyce usages: 45
Word: hath, Bible usages: 2264, Joyce usages: 39
Word: israel, Bible usages: 2565, Joyce usages: 24
Word: unto, Bible usages: 8997, Joyce usages: 15
Word: shalt, Bible usages: 1616, Joyce usages: 3

Re-display same data, in descending sort by word usage in Bible
Word: unto, Bible usages: 8997, Joyce usages: 15
Word: lord, Bible usages: 7830, Joyce usages: 447
Word: thou, Bible usages: 5474, Joyce usages: 161
Word: thy, Bible usages: 4600, Joyce usages: 141
Word: god, Bible usages: 4443, Joyce usages: 654
Word: ye, Bible usages: 3982, Joyce usages: 45
Word: thee, Bible usages: 3826, Joyce usages: 93
Word: israel, Bible usages: 2565, Joyce usages: 24
Word: son, Bible usages: 2370, Joyce usages: 326
Word: king, Bible usages: 2270, Joyce usages: 197
Word: hath, Bible usages: 2264, Joyce usages: 39
Word: people, Bible usages: 2145, Joyce usages: 237
Word: house, Bible usages: 2024, Joyce usages: 511
Word: children, Bible usages: 1802, Joyce usages: 159
Word: day, Bible usages: 1734, Joyce usages: 747
Word: land, Bible usages: 1718, Joyce usages: 249
Word: shalt, Bible usages: 1616, Joyce usages: 3
Word: hand, Bible usages: 1466, Joyce usages: 915
Word: father, Bible usages: 979, Joyce usages: 828
Word: time, Bible usages: 623, Joyce usages: 1137

Printing random 5% sample of words common in both texts
[u'swell', u'wrinkles', u'covers', u'fray', u'bastards', u'ho',
u'stealing', u'pages', u'rue', u'noon', u'wavering', u'whosoever',
[...]
u'matter', u'ointment', u'farewell', u'altar', u'fight']
```

### Problem 2. 
Implement problem 1 using DataFrame API. You could implement this problem in a command shell or as a standalone program. 
(20%)

#### Explanation
For this solution, I made heavy use of a User Defined Function for
converting a `Row` of a text line, to an array of cleaned words, then
used `explode` to create rows from those words.

I removed stop-words using a `leftanti` join, which excludes rows that
are present in both joined dataframes.

My remaining solutions involved heavy use of Spark's SQL abstractions
to select and sort depending on the requirements.

#### problem2.py
```python
import string
import re

from pyspark import SparkConf
from pyspark.sql import SparkSession
from pyspark.sql.types import ArrayType, StringType
from pyspark.sql.functions import trim, split, when, col, udf, explode

PUNCT_NO_APOST = string.punctuation.replace("'", "")
RE_SCRIPT_NUM = r'\d{2}:\d{3}:\d{3}\s'

def main(session):
    text_clean_udf = udf(clean_text_line, ArrayType(StringType()))

    stop_words_df = session.read.text('stop_words.txt')
    stop_words_df = stop_words_df.select(
        explode(text_clean_udf('value')).alias('word')
    )

    bible_df = session.read.text('bible.txt')
    bible_df = bible_df.select(
        explode(text_clean_udf('value')).alias('word')
    )
    bible_df = bible_df.join(stop_words_df, 'word', 'leftanti')

    ulysses_df = session.read.text('4300.txt')
    ulysses_df = ulysses_df.select(
        explode(text_clean_udf('value')).alias('word')
    )
    ulysses_df = ulysses_df.join(stop_words_df, 'word', 'leftanti')

    bible_counts = bible_df.groupBy('word')\
        .count().sort(col('count').desc())
    print('\nShowing top 30 most-words of Bible...')
    bible_counts.show(n=30)
    ulysses_counts = ulysses_df.groupBy('word')\
        .count().sort(col('count').desc())
    print('\nShowing top 30 most-used words in Ulysses')
    ulysses_counts.show(n=30)

    print('\nCreating dataframe of all common words, '
          'and the respective counts')
    joined_counts = bible_counts.alias('b_count')\
        .join(ulysses_counts.alias('u_count'),
              bible_counts.word == ulysses_counts.word)\
        .select(bible_counts.word,
                col('b_count.count').alias('bible_count'),
                col('u_count.count').alias('ulysses_count'))

    print('\nShowing twenty most common words across both texts')
    common_shared_words = joined_counts.alias('counts')\
        .select(col('counts.word'),
                (joined_counts.bible_count +
                 joined_counts.ulysses_count).alias('total_usage'),
                col('counts.bible_count'),
                col('counts.ulysses_count'))\
        .sort(col('total_usage').desc())
    common_shared_words.show()

    print('\nDisplaying common word counts in descending '
          'sort by word usage in Ulysses')
    joined_counts.sort(col('ulysses_count').desc()).show()

    print('\nDisplaying common word counts in descending '
          'sort by word usage in the Bible')
    joined_counts.sort(col('bible_count').desc()).show()

    print('\nDisplaying a 5% sample of common words between texts')
    print([row.word for row in
           joined_counts.sample(False, 0.05).select(col('word')).collect()])


def clean_text_line(line):
    line = re.sub(RE_SCRIPT_NUM, '', line)
    for punct in PUNCT_NO_APOST:
        line = line.replace(punct, '')
    line = line.strip().split(' ')
    line = [word.strip().lower() for word in line
            if word.strip()
            and not word.strip().isdigit()]
    return line if line else None


if __name__ == '__main__':
    app_name = 'MyApp'
    conf = SparkConf().setMaster("local").setAppName(app_name)
    spark = SparkSession.builder.appName(app_name).getOrCreate()
    main(spark)
```

#### Output
```bash
root@sandbox:/usr_data# python problem2.py
[...]

Showing top 30 most-words of Bible...
+--------+-----+
|    word|count|
+--------+-----+
|    unto| 8997|
|    lord| 7830|
|    thou| 5474|
|     thy| 4600|
|     god| 4443|
|      ye| 3982|
|    thee| 3826|
|  israel| 2565|
|     son| 2370|
|    king| 2270|
|    hath| 2264|
|  people| 2145|
|   house| 2024|
|children| 1802|
|     day| 1734|
|    land| 1718|
|   shalt| 1616|
|    hand| 1466|
|  saying| 1445|
|  behold| 1326|
|   saith| 1262|
|    hast| 1070|
|    sons| 1068|
|   david| 1015|
|   earth|  987|
|  father|  979|
|   jesus|  973|
|   thine|  938|
|    name|  930|
| thereof|  906|
+--------+-----+
only showing top 30 rows


Showing top 30 most-used words in Ulysses
+-------+-----+
|   word|count|
+-------+-----+
|  bloom| 2798|
|stephen| 1511|
|   time| 1146|
|    yes| 1082|
|   eyes|  987|
|   hand|  918|
| street|  879|
| little|  870|
| father|  831|
|    day|  753|
|  round|  717|
|  night|  696|
|   head|  666|
|    sir|  657|
|   name|  651|
|    god|  645|
|   look|  594|
|   life|  583|
|   john|  582|
|  don't|  563|
|  woman|  558|
|   poor|  558|
|   tell|  532|
|  voice|  531|
|  house|  511|
|    hat|  504|
| course|  498|
|dedalus|  495|
|   left|  495|
|  white|  489|
+-------+-----+
only showing top 30 rows


Creating dataframe of all common words, and the respective counts

Showing twenty most common words across both texts
+--------+-----------+-----------+-------------+
|    word|total_usage|bible_count|ulysses_count|
+--------+-----------+-----------+-------------+
|    unto|       9012|       8997|           15|
|    lord|       8277|       7830|          447|
|    thou|       5635|       5474|          161|
|     god|       5088|       4443|          645|
|     thy|       4741|       4600|          141|
|      ye|       4027|       3982|           45|
|    thee|       3919|       3826|           93|
|     son|       2699|       2370|          329|
|  israel|       2589|       2565|           24|
|   house|       2535|       2024|          511|
|     day|       2487|       1734|          753|
|    king|       2467|       2270|          197|
|    hand|       2384|       1466|          918|
|  people|       2382|       2145|          237|
|    hath|       2303|       2264|           39|
|    land|       1967|       1718|          249|
|children|       1961|       1802|          159|
|  father|       1810|        979|          831|
|    time|       1769|        623|         1146|
|   shalt|       1619|       1616|            3|
+--------+-----------+-----------+-------------+
only showing top 20 rows


Displaying common word counts in descending sort by word usage in Ulysses
+-------+-----------+-------------+
|   word|bible_count|ulysses_count|
+-------+-----------+-------------+
|stephen|          7|         1511|
|   time|        623|         1146|
|    yes|          4|         1082|
|   eyes|        502|          987|
|   hand|       1466|          918|
| street|         36|          879|
| little|        242|          870|
| father|        979|          831|
|    day|       1734|          753|
|  round|        320|          717|
|  night|        307|          696|
|   head|        364|          666|
|    sir|         12|          657|
|   name|        930|          651|
|    god|       4443|          645|
|   look|        155|          594|
|   life|        452|          583|
|   john|        143|          582|
|  woman|        357|          558|
|   poor|        205|          558|
+-------+-----------+-------------+
only showing top 20 rows


Displaying common word counts in descending sort by word usage in the Bible
+--------+-----------+-------------+
|    word|bible_count|ulysses_count|
+--------+-----------+-------------+
|    unto|       8997|           15|
|    lord|       7830|          447|
|    thou|       5474|          161|
|     thy|       4600|          141|
|     god|       4443|          645|
|      ye|       3982|           45|
|    thee|       3826|           93|
|  israel|       2565|           24|
|     son|       2370|          329|
|    king|       2270|          197|
|    hath|       2264|           39|
|  people|       2145|          237|
|   house|       2024|          511|
|children|       1802|          159|
|     day|       1734|          753|
|    land|       1718|          249|
|   shalt|       1616|            3|
|    hand|       1466|          918|
|  saying|       1445|          171|
|  behold|       1326|           24|
+--------+-----------+-------------+
only showing top 20 rows


Displaying a 5% sample of common words between texts
[u'thee', u'israel', u'saith', u'hast', u'thereof',
u'according', u'whom', u'hands', u'life', u'wherefore',
u'silver', u'twenty', u'cometh', u'laid', u'live', u'body',
[...]
u'send', u'truth', u'jordan', u'stones', u'ephraim']
root@sandbox:/usr_data#
```

### Problem 3. 
Consider attached files transactions.txt and products.txt. Each line in transactions.txt file contains a transaction date, time, customer id, product id, quantity bought and price paid, delimited with hash (#) sign. Each line in file products.txt contains product id, product name, unit price and quantity available in the store. Bring those data in Spark and organize it as DataFrames with named columns. Using either DataFrame methods or plain SQL statements find 5 customers with the largest spent on the day. Find the names of the products each of those 5 customers bought. Find the names and total number sold of 10 most popular products. Order products once per the number sold and then by the total value (quanity*price) sold. 
(30%)

#### Explanation
To begin this solution, I read the `products.txt` and `transactions.txt`
files with the csv reader to simplify data organization. I used a custom
schema to ensure that data could be handled properly.

I was able to accomplish each of the tasks in the problem set through
various series of `select`s, `join`s, `groupBy`s, and `sort`s. `select`
allowed me to preserve only the columns I needed, and rename columns
as needed. I used `join` to unify the transactions data with the
product data. I used `groupBy` in conjunction with `sum` to accumulate
data according to customer and product ids. Finally, I used `sort` to
display the data in a sorted manner, according to a specific column.

#### problem3.py
```python
from pyspark import SparkConf
from pyspark.sql import SparkSession
from pyspark.sql.types import (
    StringType, StructField, StructType, IntegerType, FloatType
)
from pyspark.sql.functions import col


def main(session):
    product_fields = [StructField(header, type, True) for header, type in
                      [('product_id', IntegerType()),
                       ('product_name', StringType()),
                       ('unit_price', FloatType()),
                       ('quantity', IntegerType())]]
    product_schema = StructType(product_fields)
    products_df = session.read.csv('products.txt', schema=product_schema, sep='#')

    trans_fields = [StructField(header, type, True) for header, type in
                    [('transaction_date', StringType()),
                     ('time', StringType()),
                     ('customer_id', IntegerType()),
                     ('product_id', IntegerType()),
                     ('quantity_bought', IntegerType()),
                     ('price_paid', FloatType())]]
    trans_schema = StructType(trans_fields)
    trans_df = session.read.csv('transactions.txt', schema=trans_schema, sep='#')

    print('\nLoad products.txt into memory')
    products_df.show()
    print('\nLoad transactions.txt into memory')
    trans_df.show()

    top_five_df = trans_df\
        .select(trans_df.customer_id,
                    (trans_df.quantity_bought * trans_df.price_paid).alias('transaction_total'))\
        .groupBy(trans_df.customer_id).sum('transaction_total')\
        .select(col('customer_id'), col('sum(transaction_total)').alias('total_spent'))\
        .sort(col('total_spent').desc()).limit(5)
    print('\nShowing the five top spenders')
    top_five_df.show()

    products_purchased = top_five_df.join(trans_df, ['customer_id'])\
        .select(col('customer_id'), col('product_id'))\
        .join(products_df, 'product_id')\
        .select(col('customer_id'), col('product_name'))\
        .sort(col('customer_id'))
    num_rows = products_purchased.count()
    print('\nShowing product names for top five purchasers')
    products_purchased.show(n=num_rows, truncate=False)

    most_popular = trans_df\
        .groupBy(col('product_id')).sum('quantity_bought')\
        .select(col('product_id'), col('sum(quantity_bought)').alias('total_bought'))\
        .join(products_df, ['product_id'])


    print('\nShowing the product names of the ten most popular products by number sold')
    most_popular\
        .select(col('product_name'), col('total_bought'))\
        .sort(col('total_bought').desc())\
        .show(n=10, truncate=False)

    print('\nShowing the product names of the ten most popular products by revenue')
    most_popular\
        .select(col('product_name'),
                (most_popular.total_bought * most_popular.unit_price).alias('revenue'))\
        .sort(col('revenue').desc())\
        .show(n=10, truncate=False)


if __name__ == '__main__':
    app_name = 'MyApp'
    conf = SparkConf().setMaster("local").setAppName(app_name)
    spark = SparkSession.builder.appName(app_name).getOrCreate()
    main(spark)
```

#### Output
```bash
root@sandbox:/usr_data# python problem3.py
[...]
+----------+--------------------+----------+--------+
|product_id|        product_name|unit_price|quantity|
+----------+--------------------+----------+--------+
|         1|ROBITUSSIN PEAK C...|   9721.89|      10|
|         2|Mattel Little Mom...|   6060.78|       6|
|         3|Cute baby doll, b...|   1808.79|       2|
|         4|           Bear doll|     51.06|       6|
|         5|LEGO Legends of C...|    849.36|       6|
|         6|         LEGO Castle|   4777.51|      10|
|         7|         LEGO Mixels|   8720.91|       1|
|         8|      LEGO Star Wars|   7592.44|       4|
|         9|LEGO Lord of the ...|    851.67|       2|
|        10|     LEGO The Hobbit|   7314.55|       9|
|        11|      LEGO Minecraft|   5646.81|       3|
|        12|   LEGO Hero Factory|    6911.2|       1|
|        13|   LEGO Architecture|    604.58|       5|
|        14|        LEGO Technic|   7423.48|       3|
|        15|LEGO Storage & Ac...|   3125.96|       2|
|        16|        LEGO Classic|    9933.3|      10|
|        17|   LEGO Galaxy Squad|   5593.16|       4|
|        18|     LEGO Mindstorms|   6022.88|      10|
|        19|    LEGO Minifigures|   5775.99|       1|
|        20|          LEGO Elves|   4589.79|       4|
+----------+--------------------+----------+--------+
only showing top 20 rows

+----------------+--------+-----------+----------+---------------+----------+
|transaction_date|    time|customer_id|product_id|quantity_bought|price_paid|
+----------------+--------+-----------+----------+---------------+----------+
|      2015-03-30| 6:55 AM|         51|        68|              1|   9506.21|
|      2015-03-30| 7:39 PM|         99|        86|              5|   4107.59|
|      2015-03-30|11:57 AM|         79|        58|              7|   2987.22|
|      2015-03-30|12:46 AM|         51|        50|              6|   7501.89|
|      2015-03-30|11:39 AM|         86|        24|              5|    8370.2|
|      2015-03-30|10:35 AM|         63|        19|              5|   1023.57|
|      2015-03-30| 2:30 AM|         23|        77|              7|   5892.41|
|      2015-03-30| 7:41 PM|         49|        58|              4|   9298.18|
|      2015-03-30| 9:18 AM|         97|        86|              8|   9462.89|
|      2015-03-30|10:06 PM|         94|        26|              4|   4199.15|
|      2015-03-30|10:57 AM|         91|        18|              1|   3795.73|
|      2015-03-30| 7:43 AM|         20|        86|             10|   1477.35|
|      2015-03-30| 5:58 PM|         38|        39|              6|    1090.0|
|      2015-03-30| 1:08 PM|         46|         6|             10|   1014.78|
|      2015-03-30|12:18 AM|         56|        48|              9|   8346.42|
|      2015-03-30| 1:18 AM|         11|        58|              4|    364.59|
|      2015-03-30| 3:01 AM|         59|         9|              5|   5984.68|
|      2015-03-30|11:44 AM|          8|        35|              6|    1859.2|
|      2015-03-30|12:05 PM|         23|         8|              3|   1527.04|
|      2015-03-30| 4:10 AM|         85|        93|              9|   3314.71|
+----------------+--------+-----------+----------+---------------+----------+
only showing top 20 rows


Showing the five top spenders
+-----------+------------------+
|customer_id|       total_spent|
+-----------+------------------+
|         56| 597122.3084716797|
|         31|   540828.01953125|
|         26|  513581.830078125|
|         76|505617.60693359375|
|          2| 476018.0302734375|
+-----------+------------------+


Showing product names for top five purchasers
+-----------+----------------------------------------------------+
|customer_id|product_name                                        |
+-----------+----------------------------------------------------+
|2          |CVS                                                 |
|2          |Essentials Dantes Inferno PSP                       |
|2          |Intel Core i5 3570                                  |
|2          |ROBITUSSIN PEAK COLD NIGHTTIME COLD PLUS FLU        |
|2          |LG LED TV 42LA6130                                  |
|2          |PC HP 490PD MT, D5T62EA                             |
|2          |Roller Derby Roller Street Series Skateboard Bruiser|
|2          |Disposable diapers                                  |
|2          |LG LED TV 32LN5400                                  |
|2          |LEGO Castle                                         |
|2          |LEGO Legends of Chima                               |
|2          |LG LED TV 42LA6130                                  |
|2          |Alphanate                                           |
|2          |LEGO Storage & Accessories                          |
|2          |Grippe                                              |
|26         |Essentials Tekken 6 PS3                             |
|26         |GAM X360  Dead Space 3 Limited Edition              |
|26         |Obao                                                |
|26         |Procesor Intel Core i5 3470                         |
|26         |Notebook Lenovo E545 20B2000KSC                     |
|26         |LG LED TV 32LN575S                                  |
|26         |Caldyphen Clear                                     |
|26         |GAM X360 Assassins Creed 3                          |
|26         |Niacin                                              |
|26         |Far Cry 4 Limited Edition for Xbox One              |
|26         |Glipizide                                           |
|31         |Scrub Care Povidone Iodine Cleansing Scrub          |
|31         |Caldyphen Clear                                     |
|31         |AMBROSIA TRIFIDA POLLEN                             |
|31         |Star Wars Republic Commando PC                      |
|31         |Caldyphen Clear                                     |
|31         |Notebook Lenovo U330p, 59-390439                    |
|31         |Stomach Disorders                                   |
|31         |Might and Magic: Clash Of Heroes PC                 |
|31         |LEGO Speed Champion                                 |
|31         |Notebook Lenovo U330p, 59-390439                    |
|31         |PC HP 490PD MT, D5T60EA                             |
|31         |Toothbrush                                          |
|31         |ROBITUSSIN PEAK COLD NIGHTTIME COLD PLUS FLU        |
|31         |Essentials Dantes Inferno PSP                       |
|56         |Notebook Lenovo E545 20B2000KSC                     |
|56         |Glipizide                                           |
|56         |Dictionary                                          |
|56         |Roller Derby Roller Street Series Skateboard Bruiser|
|56         |Barbie Beach Ken Doll                               |
|56         |SAMSUNG LED TV 39F5500, Full HD, USB                |
|56         |Levalbuterol Hydrochloride                          |
|56         |Essentials Medal of Honor: Heroes 2 PSP             |
|56         |Jafra                                               |
|56         |Notebook Lenovo U430p, 59-390459                    |
|56         |Grippe                                              |
|56         |Far Cry 4 Limited Edition for Xbox One              |
|56         |Notebook Lenovo Yoga Think, 20CD0001SC              |
|56         |PC HP 490PD MT, D5T68EA                             |
|56         |Notebook Lenovo U430p, 59-390459                    |
|56         |PC HP 490PD MT, D5T60EA                             |
|56         |LG LED TV 42LA6130                                  |
|76         |Notebook Lenovo U330p, 59-390439                    |
|76         |GAM X360 Need for Speed Most Wanted 2012            |
|76         |LEGO Bionicle                                       |
|76         |GUNA-EGF                                            |
|76         |Jafra                                               |
|76         |LG LED TV 32LN5400                                  |
|76         |LEGO Hero Factory                                   |
|76         |Notebook Lenovo U330p, 59-390439                    |
|76         |Notebook Lenovo U430p, 59-390459                    |
|76         |Glipizide                                           |
|76         |Notebook Lenovo U330p, 59-390439                    |
|76         |healthy accents sinus                               |
|76         |Jafra                                               |
|76         |Jafra                                               |
|76         |Roller Derby Roller Street Series Skateboard Bruiser|
+-----------+----------------------------------------------------+


Showing the product names of the ten most popular products by number sold
+----------------------------------------------------+------------+
|product_name                                        |total_bought|
+----------------------------------------------------+------------+
|Notebook Lenovo U330p, 59-390439                    |226         |
|SAMSUNG LED TV 39F5500, Full HD, USB                |142         |
|Jantoven                                            |102         |
|Jafra                                               |102         |
|Far Cry 4 Limited Edition for Xbox One              |101         |
|Roller Derby Roller Street Series Skateboard Bruiser|91          |
|Procesor Intel Core i5 3470                         |90          |
|Sony Playstation 3                                  |88          |
|chest congestion                                    |84          |
|Barbie Beach Ken Doll                               |82          |
+----------------------------------------------------+------------+
only showing top 10 rows


Showing the product names of the ten most popular products by revenue
+----------------------------------------------------+---------+
|product_name                                        |revenue  |
+----------------------------------------------------+---------+
|ROBITUSSIN PEAK COLD NIGHTTIME COLD PLUS FLU        |729141.75|
|Roller Derby Roller Street Series Skateboard Bruiser|708324.9 |
|LEGO Mixels                                         |688951.9 |
|LG LED TV 32LN575S                                  |670394.4 |
|Gabapentin                                          |587159.2 |
|LEGO Speed Champion                                 |560103.7 |
|LEGO Classic                                        |556264.8 |
|ibuprofen                                           |545597.5 |
|GAM X360 Need for Speed Most Wanted 2012            |543217.6 |
|Glipizide                                           |534457.1 |
+----------------------------------------------------+---------+
only showing top 10 rows

root@sandbox:/usr_data#
```

### Problem 4. 
Implement problem 3 using RDD APIs.
(20%)

#### Explanation
I was not able to complete problem 4 due to running out of time.

