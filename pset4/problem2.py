import string
import re

from pyspark import SparkConf
from pyspark.sql import SparkSession
from pyspark.sql.types import ArrayType, StringType
from pyspark.sql.functions import trim, split, when, col, udf, explode

PUNCT_NO_APOST = string.punctuation.replace("'", "")
RE_SCRIPT_NUM = r'\d{2}:\d{3}:\d{3}\s'

def main(session):
    text_clean_udf = udf(clean_text_line, ArrayType(StringType()))

    stop_words_df = session.read.text('stop_words.txt')
    stop_words_df = stop_words_df.select(
        explode(text_clean_udf('value')).alias('word')
    )

    bible_df = session.read.text('bible.txt')
    bible_df = bible_df.select(
        explode(text_clean_udf('value')).alias('word')
    )
    bible_df = bible_df.join(stop_words_df, 'word', 'leftanti')

    ulysses_df = session.read.text('4300.txt')
    ulysses_df = ulysses_df.select(
        explode(text_clean_udf('value')).alias('word')
    )
    ulysses_df = ulysses_df.join(stop_words_df, 'word', 'leftanti')

    bible_counts = bible_df.groupBy('word')\
        .count().sort(col('count').desc())
    print('\nShowing top 30 most-words of Bible...')
    bible_counts.show(n=30)
    ulysses_counts = ulysses_df.groupBy('word')\
        .count().sort(col('count').desc())
    print('\nShowing top 30 most-used words in Ulysses')
    ulysses_counts.show(n=30)

    print('\nCreating dataframe of all common words, '
          'and the respective counts')
    joined_counts = bible_counts.alias('b_count')\
        .join(ulysses_counts.alias('u_count'),
              bible_counts.word == ulysses_counts.word)\
        .select(bible_counts.word,
                col('b_count.count').alias('bible_count'),
                col('u_count.count').alias('ulysses_count'))

    print('\nShowing twenty most common words across both texts')
    common_shared_words = joined_counts.alias('counts')\
        .select(col('counts.word'),
                (joined_counts.bible_count +
                 joined_counts.ulysses_count).alias('total_usage'),
                col('counts.bible_count'),
                col('counts.ulysses_count'))\
        .sort(col('total_usage').desc())
    common_shared_words.show()

    print('\nDisplaying common word counts in descending '
          'sort by word usage in Ulysses')
    joined_counts.sort(col('ulysses_count').desc()).show()

    print('\nDisplaying common word counts in descending '
          'sort by word usage in the Bible')
    joined_counts.sort(col('bible_count').desc()).show()

    print('\nDisplaying a 5% sample of common words between texts')
    print([row.word for row in
           joined_counts.sample(False, 0.05).select(col('word')).collect()])


def clean_text_line(line):
    line = re.sub(RE_SCRIPT_NUM, '', line)
    for punct in PUNCT_NO_APOST:
        line = line.replace(punct, '')
    line = line.strip().split(' ')
    line = [word.strip().lower() for word in line
            if word.strip()
            and not word.strip().isdigit()]
    return line if line else None


if __name__ == '__main__':
    app_name = 'MyApp'
    conf = SparkConf().setMaster("local").setAppName(app_name)
    spark = SparkSession.builder.appName(app_name).getOrCreate()
    main(spark)


