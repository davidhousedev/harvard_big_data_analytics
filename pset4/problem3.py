from pyspark import SparkConf
from pyspark.sql import SparkSession
from pyspark.sql.types import (
    StringType, StructField, StructType, IntegerType, FloatType
)
from pyspark.sql.functions import col


def main(session):
    product_fields = [StructField(header, type, True) for header, type in
                      [('product_id', IntegerType()),
                       ('product_name', StringType()),
                       ('unit_price', FloatType()),
                       ('quantity', IntegerType())]]
    product_schema = StructType(product_fields)
    products_df = session.read.csv('products.txt', schema=product_schema, sep='#')

    trans_fields = [StructField(header, type, True) for header, type in
                    [('transaction_date', StringType()),
                     ('time', StringType()),
                     ('customer_id', IntegerType()),
                     ('product_id', IntegerType()),
                     ('quantity_bought', IntegerType()),
                     ('price_paid', FloatType())]]
    trans_schema = StructType(trans_fields)
    trans_df = session.read.csv('transactions.txt', schema=trans_schema, sep='#')

    print('\nLoad products.txt into memory')
    products_df.show()
    print('\nLoad transactions.txt into memory')
    trans_df.show()

    top_five_df = trans_df\
        .select(trans_df.customer_id,
                    (trans_df.quantity_bought * trans_df.price_paid).alias('transaction_total'))\
        .groupBy(trans_df.customer_id).sum('transaction_total')\
        .select(col('customer_id'), col('sum(transaction_total)').alias('total_spent'))\
        .sort(col('total_spent').desc()).limit(5)
    print('\nShowing the five top spenders')
    top_five_df.show()

    products_purchased = top_five_df.join(trans_df, ['customer_id'])\
        .select(col('customer_id'), col('product_id'))\
        .join(products_df, 'product_id')\
        .select(col('customer_id'), col('product_name'))\
        .sort(col('customer_id'))
    num_rows = products_purchased.count()
    print('\nShowing product names for top five purchasers')
    products_purchased.show(n=num_rows, truncate=False)

    most_popular = trans_df\
        .groupBy(col('product_id')).sum('quantity_bought')\
        .select(col('product_id'), col('sum(quantity_bought)').alias('total_bought'))\
        .join(products_df, ['product_id'])


    print('\nShowing the product names of the ten most popular products by number sold')
    most_popular\
        .select(col('product_name'), col('total_bought'))\
        .sort(col('total_bought').desc())\
        .show(n=10, truncate=False)

    print('\nShowing the product names of the ten most popular products by revenue')
    most_popular\
        .select(col('product_name'),
                (most_popular.total_bought * most_popular.unit_price).alias('revenue'))\
        .sort(col('revenue').desc())\
        .show(n=10, truncate=False)


if __name__ == '__main__':
    app_name = 'MyApp'
    conf = SparkConf().setMaster("local").setAppName(app_name)
    spark = SparkSession.builder.appName(app_name).getOrCreate()
    main(spark)
