docker run \
    -it \
    --hostname=quickstart.cloudera \
    --privileged=true \
    -v $(pwd):/user_data \
    --workdir=/user_data \
    cloudera/quickstart \
    /usr/bin/docker-quickstart