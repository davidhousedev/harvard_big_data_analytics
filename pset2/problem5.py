# Cypher commands for creating CSV files, then downloaded from browser

# # actors.csv
# MATCH (person:Actor)
# RETURN id(person) as id, person.name as name

# # directors.csv
# MATCH (person:Director)
# RETURN id(person) as id, person.name as name

# # movies.csv
# MATCH (movie:Movie)
# RETURN id(movie) as id, movie.title as title, movie.year as year

# # director_roles.csv
# MATCH (director)-[:DIRECTED]->(movie)
# RETURN id(director) as director_id, id(movie) as movie_id

# # movie_roles.csv
# MATCH (actor)-[rel:ACTED_IN]->(movie)
# RETURN id(actor) as actor_id, id(movie) as movie_id, rel.role as role

from subprocess import run

from retrying import retry

from neo4j.v1 import GraphDatabase, basic_auth

NEO4J_IMPORT_PATH = '/var/lib/neo4j/import'


print('\nBeginning Problem 5')

# Start Up fresh neo4j with CSVs mounted within docker container
run(f"""docker run \
   --rm \
   --detach \
   --publish=7474:7474 \
   --publish=7687:7687 \
   --env NEO4J_AUTH=none \
   -v $(pwd)/csvs:{NEO4J_IMPORT_PATH} \
   neo4j""", shell=True)

@retry(wait_fixed=1000)
def establish_db_driver():
    print('Attempting to establish database connection...')
    return GraphDatabase.driver(
        "bolt://localhost:7687", auth=basic_auth("neo4j", "david")
    )

driver = establish_db_driver()

# Utilize Python context manager to open and close session
with driver.session() as session:
    session.run(
        """
        LOAD CSV WITH HEADERS FROM "file:///actors.csv" AS line
        CREATE (actor:Actor {id: line.id, name: line.name})
        """)
    session.run(
        """
        LOAD CSV WITH HEADERS FROM "file:///directors.csv" AS line
        CREATE (director:Director {id: line.id, name: line.name})
        """)
    session.run(
        """
        LOAD CSV WITH HEADERS FROM "file:///movies.csv" AS line
        CREATE (movie:Movie {id: line.id, title: line.title, year: line.year})
        """)
    session.run(
        """
        LOAD CSV WITH HEADERS FROM "file:///director_roles.csv" AS line
        MATCH (director:Director {id:line.director_id})
        MATCH (movie:Movie {id: line.movie_id})
        MERGE (director)-[:DIRECTED]->(movie)
        """)
    session.run(
        """
        LOAD CSV WITH HEADERS FROM "file:///movie_roles.csv" AS line
        MATCH (actor:Actor {id:line.actor_id})
        MATCH (movie:Movie {id: line.movie_id})
        MERGE (actor)-[:ACTED_IN {role: line.role}]->(movie)
        """)

    for record in session.run(
            """
            MATCH (person)-[rel]->(movie)
            RETURN person.name as name,
                   type(rel) as relationship,
                   movie.title as movie
            """):
        print(f'Created record that {record["name"]} '
              f'{record["relationship"]} {record["movie"]}')
