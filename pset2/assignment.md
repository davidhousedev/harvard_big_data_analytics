HU Extension              Assignment 02       E63 Big Data Analytics 		                  	
Handed out: 09/08/2017                                    Due by 11:59 AM, Saturday, 09/16/2017
 
Please, describe every step of your work and present all intermediate and final results in a Word document. Please, copy past text version of all essential command and snippets of results into the Word document with explanations of the purpose of those commands. We cannot retype text that is in JPG images. Please, always submit a separate copy of the original, working scripts and/or class files you used. Sometimes we need to run your code and retyping is too costly. Please include in your MS Word document only relevant portions of the console output or output files. Sometime either console output or the result file is too long and including it into the MS Word document makes that document too hard to read. PLEASE DO NOT EMBED files into your MS Word document. For issues and comments visit the class Discussion Board on Piazza. 

You can do most of this assignment in Python, Java, R, Scala or any other language of your convenience.

## Problem 1.
The following is the content of Movies database. Bring that database into Neo4J using curl
```
CREATE (matrix1:Movie { title : 'The Matrix', year : '1999-03-31' }) return id(matrix1)
CREATE (matrix2:Movie { title : 'The Matrix Reloaded', year : '2003-05-07' }) return id(matrix2)
CREATE (matrix3:Movie { title : 'The Matrix Revolutions', year : '2003-10-27' }) return id(matrix3)
CREATE (keanu:Actor { name:'Keanu Reeves' }) return id(Keanu)
CREATE (laurence:Actor { name:'Laurence Fishburne' })
CREATE (carrieanne:Actor { name:'Carrie-Anne Moss' })
CREATE (keanu)-[:ACTS_IN { role : 'Neo' }]->(matrix1)
CREATE (keanu)-[:ACTS_IN { role : 'Neo' }]->(matrix2)
CREATE (keanu)-[:ACTS_IN { role : 'Neo' }]->(matrix3)
CREATE (laurence)-[:ACTS_IN { role : 'Morpheus' }]->(matrix1)
CREATE (laurence)-[:ACTS_IN { role : 'Morpheus' }]->(matrix2)
CREATE (laurence)-[:ACTS_IN { role : 'Morpheus' }]->(matrix3)
CREATE (carrieanne)-[:ACTS_IN { role : 'Trinity' }]->(matrix1)
CREATE (carrieanne)-[:ACTS_IN { role : 'Trinity' }]->(matrix2)
CREATE (carrieanne)-[:ACTS_IN { role : 'Trinity' }]->(matrix3)
```

You might want to create a bash script which contains curl command with previous Cypher request and run that script from Cygwin or Linux (Unix) prompt.  Following notes could be helpful:
1) you might want to use notepad++ to create and edit the bash file, and go to edit --> EOL conversion --> Unix.
2) Specify the user name and password like “-user neo4j:neo4jneo4j" before the local host url.
3) Add one space and one \ at each point you want to have a line break.
4) In the string of query, every double quote " should be changed to \".
(20%)
## Problem 2.
Keanu Reeves acted in the movie “John Wick” which is not in the database. That movie was directed by Chad Stahelski and David Leitch. Cast of the movie included William Dafoe and Michael Nyquist. Demonstrate that you have successfully brought data about John Wick movie into the database. You can use Cypher Browser or any other means. Delete above movie and all the cast except Keanu Reeves.
(15%)
## Problem 3.
Add all the actors and the roles they played in this movie “John Wick” to the database using JAVA REST API or some other APIs for Neo4J in a language of your choice (not Curl). Demonstrate that you have successfully brought data about John Wick movie into the database. You can use Cypher Browser or any other means.
(15%)
## Problem 4.
Find a list of actors playing in movies in which Keanu Reeves played. Find directors of movies in which K. Reeves played. Please use any language of your convenience (Java, Python, C#, R, curl). Verify your results using Cypher queries in Cypher Browser
(15%)

## Problem 5.
Find a way to export data from Neo4j into a set of CSV files. Delete your database and demonstrate that you can recreate the database by loading those CSV files. Please use any programming language of your convenience: Java, Python, R, C# or Scala.
(20%)
## Problem 6.
Find a way to use Arrow Tool (http://www.apcjones.com) to paint a relationship between a dog and his owner who live in New York and walk through the Central Park on Sunday afternoon. Add Labels and necessary properties to all nodes and relationships. Export your graph in Cypher format and then adjust (if necessary) generated Cypher so that you can create that graph in Neo4J database. Verify that your graph is indeed created using Cypher Browser.
(15%)