from neo4j.v1 import GraphDatabase, basic_auth

driver = GraphDatabase.driver("bolt://localhost:7687", auth=basic_auth("neo4j", "david"))

# Utilize Python context manager to open and close session
print('\nBeginning Problem 2')
with driver.session() as session:
    # Create new Movie node for John Wick
    result = session.run(
        "CREATE (wick:Movie {title: 'John Wick', year: '2014-10-24'}) "
        "RETURN wick.title as title"
    )
    for movie in result:
        print(f'Created movie: {movie["title"]}')

    # Create Director nodes for John Wick directors
    director_names = ['Chad Stahelski', 'David Leitch']
    result = []
    for name in director_names:
        result += session.run(
            "MATCH (wick:Movie {title: 'John Wick'}) "
            "CREATE (dir:Director {name: {name}})-[:DIRECTED]->(wick) "
            "RETURN dir.name as name",
            {"name": name}
        )
    for director in result:
        print(f'Created director: {director["name"]}')

    cast_names = ['William Dafoe', 'Michael Nyquist']
    result = []
    for name in director_names:
        result += session.run(
            "MATCH (wick:Movie {title: 'John Wick'}) "
            "CREATE (actor:Actor {name: {name}})-[:ACTED_IN]->(wick) "
            "RETURN actor.name as name",
            {"name": name}
        )
    for actor in result:
        print(f'Created actor: {actor["name"]}')

    # Link Keanu Reeves to actor role in John Wick
    result = session.run(
        "MATCH (keanu:Actor {name: 'Keanu Reeves'}), "
              "(wick:Movie {title: 'John Wick'}) "
        "CREATE (keanu)-[:ACTED_IN]->(wick) "
        "RETURN keanu.name as name, wick.title as movie"
    )
    for record in result:
        print(f'Created link between {record["name"]} and {record["movie"]}')

    print('Deleting John Wick and all cast except for Keanu Reeves')
    session.run(
        "MATCH (movie:Movie {title: 'John Wick'}), "
        "(director:Director)-[:DIRECTED]->(movie), "
        "(actor:Actor)-[:ACTED_IN]->(movie) "
        "WHERE NOT actor.name STARTS WITH 'Keanu Reeves' "
        "DETACH DELETE actor, director, movie")
