#!/usr/bin/env bash

#############
# Problem 1 #
#############

# Start up Neo4j using Docker
#docker run \
#    --detach \
#    --publish=7474:7474 \
#    --publish=7687:7687 \
#    --env NEO4J_AUTH=neo4j/david \
#    neo4j


# Execute all creates within one statement, to preserve node names (e.g. keanu) in the current namespace
json='
{
	"statements": [{
		"statement": "CREATE (matrix1:Movie { title : \"The Matrix\", year : \"1999-03-31\" }), (matrix2:Movie { title : \"The Matrix Reloaded\", year : \"2003-05-07\" }), (matrix3:Movie { title : \"The Matrix Revolutions\", year : \"2003-10-27\" }), (keanu:Actor { name:\"Keanu Reeves\" }), (laurence:Actor { name:\"Laurence Fishburne\" }), (carrieanne:Actor { name:\"Carrie-Anne Moss\" }), (keanu)-[:ACTED_IN { role : \"Neo\" }]->(matrix1), (keanu)-[:ACTED_IN { role : \"Neo\" }]->(matrix2), (keanu)-[:ACTED_IN { role : \"Neo\" }]->(matrix3), (laurence)-[:ACTED_IN { role : \"Morpheus\" }]->(matrix1), (laurence)-[:ACTED_IN { role : \"Morpheus\" }]->(matrix2), (laurence)-[:ACTED_IN { role : \"Morpheus\" }]->(matrix3), (carrieanne)-[:ACTED_IN { role : \"Trinity\" }]->(matrix1), (carrieanne)-[:ACTED_IN { role : \"Trinity\" }]->(matrix2), (carrieanne)-[:ACTED_IN { role : \"Trinity\" }]->(matrix3)"
	}]
}
'

# Post JSON payload to neo4j database by piping it to STDIN
# and reading the data from STDIN
echo "Beginning Problem 1..."
echo $json | curl -X POST http://localhost:7474/db/data/transaction/commit \
    --user "neo4j:david" \
    -H "Content-type: application/json" \
    -H "Accept: application/json" \
    -d @-
