from neo4j.v1 import GraphDatabase, basic_auth

driver = GraphDatabase.driver("bolt://localhost:7687", auth=basic_auth("neo4j", "david"))

# Utilize Python context manager to open and close session
print('\nBeginning Problem 6')
with driver.session() as session:
    session.run(
        """
        CREATE 
          (`0` :Dog {name:'Zeus',breed:'West Highland Terrier',age:'5',weight_in_lbs:'25'}) ,
          (`1` :Person {name:'Chad',occupation:'Dog Trainer',age:'25'}) ,
          (`2` :Park {name:'Central Park'}) ,
          (`3` :City {name:'New York City'}) ,
          (`1`)-[:OWNS {years: 5}]->(`0`),
          (`1`)-[:WALKED_IN {date: '2017.9.9 14.00 EDT'}]->(`2`),
          (`0`)-[:WALKED_IN {date: '2017.9.9 14.00 EDT'}]->(`2`),
          (`1`)-[:LIVES_IN {years: 25}]->(`3`),
          (`2`)-[:LOCATED_IN]->(`3`)
        """)