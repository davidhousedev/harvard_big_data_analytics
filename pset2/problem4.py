from neo4j.v1 import GraphDatabase, basic_auth

driver = GraphDatabase.driver("bolt://localhost:7687", auth=basic_auth("neo4j", "david"))

# Utilize Python context manager to open and close session
print('\nBeginning Problem 4')
with driver.session() as session:
    for record in session.run(
        """
        MATCH (keanu:Actor {name: "Keanu Reeves"}),
        (keanu)-[:ACTED_IN]->(keanu_movie:Movie),
        (co_actor:Actor)-[:ACTED_IN]->(keanu_movie)
        RETURN collect(co_actor.name) as names
        """):
        print(f'Found the names of co-actors of Keanu Reeves: '
              f'{record["names"]}')
