from neo4j.v1 import GraphDatabase, basic_auth

driver = GraphDatabase.driver("bolt://localhost:7687", auth=basic_auth("neo4j", "david"))

# Utilize Python context manager to open and close session
actor_character_map = {
	"Keanu Reeves": "John Wick",
	"Michael Nyqvist": "Viggo Tarasov",
	"Alfie Allen": "Iosef Tarasov",
	"Willem Dafoe": "Marcus",
	"Dean Winters": "Avi",
	"Adrianne Palicki": "Ms. Perkins",
	"Omer Barnea": "Gregori",
}

directors = ['Chad Stahelski', 'David Leitch']

print('\nBeginning Problem 3')
with driver.session() as session:
    for actor, character in actor_character_map.items():
        for record in session.run(
                'MERGE (wick:Movie {title: "John Wick"}) '
                'MERGE (actor:Actor {name: $actor_name}) '
                'MERGE (actor)-[role:ACTED_IN {role: $character_name}]->(wick) '
                'RETURN actor.name as actor, role.name as role, wick.title as movie',
                actor_name=actor,
                character_name=character):
            print(f"Added record for {record['actor']}'s role "
                  f"of {record['role']} in {record['movie']}")
    for director in directors:
        for record in session.run(
                'MERGE (wick:Movie {title: "John Wick"}) '
                'MERGE (director:Director {name: $name}) '
                'MERGE (director)-[:DIRECTED]->(wick) '
                'RETURN director.name as name, wick.title as movie',
                name=director):
            print(f"Created record of director {record['name']} "
                  f"for movie {record['movie']}")

    for record in session.run(
            'MATCH (keanu:Actor {name: "Keanu Reeves"})-[:ACTED_IN]->(movie:Movie)'
            'MATCH (co_actor:Actor)-[:ACTED_IN]->(movie) '
            'RETURN co_actor.name as name, movie.title as movie'):
        print(f'Found co-actor of Keanu, {record["name"]}, in {record["movie"]}')
