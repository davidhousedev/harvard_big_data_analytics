from pyspark import SparkConf
from pyspark.sql import SparkSession

app_name = 'MyApp'
conf = SparkConf().setMaster("local").setAppName(app_name)
spark = SparkSession.builder.appName(app_name).getOrCreate()

dset = spark.read.text("ulysses10.txt")
print(dset.count())
