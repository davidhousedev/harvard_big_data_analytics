# Big Data Analytics
## Problem Set 3
#### David House

## Problem 1.

Create your own Virtual Machine with a Linux operating system. The lecture notes speak about CentOS. You are welcome to work with another Linux OS. When creating the VM, create an administrative user. Call that user whatever you feel like. Please record the password of the new user. Once the VM is created transfer the attached text file Ulysses10.txt to the home of new user. You can do it using scp (secure copy command) or email. Examine the version of Java, Python and Scala on your VM. If any of those versions is below requirements for Spark 2.2 install proper version. Set JAVA_HOME environmental variable. Set your PATH environmental variable properly, so that you can invoke: java, sbt and python commands from any directory on your system.  [20%]

I decided to use Docker to host my Spark server, as it allowed for simplified initial configuration and reuse of configuration. I created a bash script to automate building and running of the container. It builds the Docker image, then starts it with the files from the current directory mapped to `/usr_data` within the container.

_start_docker_container.sh_
```bash
docker build --tag pset3-spark .

docker run -it \
    -p 8088:8088 \
    -p 8042:8042 \
    -p 4040:4040 \
    -h sandbox \
    -v $(pwd):/usr_data/ \
    pset3-spark
```

_Dockerfile_
```docker
FROM epahomov/docker-spark

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get clean

RUN pip install --upgrade pip && \
    pip install pyspark

WORKDIR /usr_data

ENTRYPOINT ["bash"]
```

Upon opening the container, I could verify that Python and Java were installed. The container did not have a full scala build, but I skipped adding that to my Docker image as Spark was already built and functional.
```bash
root@sandbox:/spark# python --version
Python 2.7.12
root@sandbox:/spark# java
Usage: java [-options] class [args...]
           (to execute a class)
   or  java [-options] -jar jarfile [args...]
           (to execute a jar file)
[...]
```


## Problem 2.

Install Spark 2.2 on your VM. Make sure that pyspark is also installed. Demonstrate that you can successfully open spark-shell and that you can eliminate most of WARNing messages. [15%]

I am able to start PySpark from bash and receive minimal warnings:
```bash
 $ ./start_docker_container.sh 
[...]
Successfully tagged pset3-spark:latest
root@sandbox:/spark# pyspark
Python 2.7.12 (default, Nov 19 2016, 06:48:10) 
[GCC 5.4.0 20160609] on linux2
Type "help", "copyright", "credits" or "license" for more information.
Using Spark's default log4j profile: org/apache/spark/log4j-defaults.properties
Setting default log level to "WARN".
To adjust logging level use sc.setLogLevel(newLevel). For SparkR, use setLogLevel(newLevel).
17/09/22 04:47:00 WARN NativeCodeLoader: Unable to load native-hadoop library for your platform
... using builtin-java classes where applicable
17/09/22 04:47:20 WARN ObjectStore: Version information not found in metastore. hive.metastore.
schema.verification is not enabled so recording the schema version 1.2.0
17/09/22 04:47:20 WARN ObjectStore: Failed to get database default, returning NoSuchObjectException
17/09/22 04:47:23 WARN ObjectStore: Failed to get database global_temp, returning NoSuchObjectException
Welcome to
      ____              __
     / __/__  ___ _____/ /__
    _\ \/ _ \/ _ `/ __/  '_/
   /__ / .__/\_,_/_/ /_/\_\   version 2.1.0
      /_/

Using Python version 2.7.12 (default, Nov 19 2016 06:48:10)
SparkSession available as 'spark'.
>>> 
```


## Problem 3.

Find the number of lines in the text file ulysses10.txt that contain word “afternoon” or “night”  or “morning”. In this problem use RDD API.  Do this in two ways, first create a lambda function which will test whether a line contains any one of those 3 words. Second, create a named function in the language of choice that returns TRUE if a line passed to it contains any one of those three words. Demonstrate that the count is the same. Use pyspark and Spark Python API. If convenient you are welcome to implement this problem in any other language: Scala, Java or R. [15%]

To solve this, I first created a simple function to iterate through a list of times, and return True if the time is found in the line. Then, after creating an RDD of the words in `ulysses10.txt`, and filtering that RDD to create a new RDD with only the required lines, I counted those lines to produce 48. I was able to use the same `textFile` RDD to filter differently, passing in a lambda function which passed a list comprehension into `any()` to accomplish the same task.

```bash
./start_docker_container.sh 
[...]
root@sandbox:/usr_data# pyspark
[...]
>>> def time_of_day_in_line(line):
...     for time in ['morning', 'afternoon', 'night']:
...         if time not in line:
...             continue
...         return True
... 
>>> textFile = sc.textFile("ulysses10.txt")
>>> linesWithTime = textFile.filter(time_of_day_in_line)
>>> linesWithTime.count()
418
>>> linesWithTime = textFile.filter(
...    lambda line: any(
...        [time in line for time in ['morning', 'afternoon', 'night']]
...    )
... )
>>> linesWithTime.count()
418                                                                             
```


## Problem 4.

Implement the above task, finding the number of lines with one of those three words in file ulysses10.txt using Dataset/DataFrame API. Again, use the language of your choice.  [20%]

To solve this, 

```python
>>> dset = spark.read.text('ulysses10.txt')
>>> count = 0
>>> for row in dset.collect():
...     if not any([time in row.value for time in ['morning', 'afternoon', 'night']]):
...         continue
...     count += 1
... 
>>> count
418
```


## Problem 5.

Create a standalone Python script that will count all words in file ulysses10.txt. You are expected to produce a single number. Do it using RDD API. If convenient, you are welcome to implement this problem in other languages: Scala, Java or R.  [%15]

For this solution, I created a script that configured its own Spark context without having to rely on the automatic configuration done when running the pyspark binary. Once my context was configured, I utilized similar patterns from previous problems to count the words in Ulysess using the RDD API.

_rdd_count.py_
```python
from pyspark import SparkContext

sc = SparkContext(appName='MyApp')

textFile = sc.textFile('ulysses10.txt')
print(textFile.count())
```


## Problem 6.

Create a standalone Python script that will count all words in file ulysses10.txt. You are expected to produce a single number. Do it using Dataset/DataFrame API. If convenient, you are welcome to implement this problem in other languages: Scala, Java or R. [%15]

The only difference between this solution and the previous solution was that I needed to configure a `SparkSession` which could read a `DataFrame` from the input file. Once my script had a configured session, I used logic from previous problems to count words using the DataFrame API.

_dataframe_count.py_
```python
from pyspark import SparkConf
from pyspark.sql import SparkSession

app_name = 'MyApp'
conf = SparkConf().setMaster("local").setAppName(app_name)
spark = SparkSession.builder.appName(app_name).getOrCreate()

dset = spark.read.text("ulysses10.txt")
print(dset.count())
```