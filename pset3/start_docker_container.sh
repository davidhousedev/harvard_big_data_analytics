docker build --tag pset3-spark .

docker run -it \
    -p 8088:8088 \
    -p 8042:8042 \
    -p 4040:4040 \
    -h sandbox \
    -v $(pwd):/usr_data/ \
    pset3-spark
